<?php

    //Get value from terminal
    $number = $argv[1];

    $romanNumber = "";

    // Order Roman number to digits number
    $romanNumerals = array (

        1 => "I",
        4 => "IV",
        5 => "V",
        9 => "IX",
        10 => "X",
        40 => "XL",
        50 => "L",
        90 => "XC",
        100 => "C",
        400 => "CD",
        500 => "D",
        900 => "CM",
        1000 => "M"

    );

    //Checking input value is a number
    if(!is_numeric($number)) {

        echo "This is not a number :(";
        exit();
    } 

    function Changer($compareItem) {

        global $number;
        global $romanNumber;
        global $romanNumerals;

        $isgreaterthan = true;

        while($isgreaterthan) {

            if($number >= $compareItem) {
    
                $number -= $compareItem;
                $romanNumber .= $romanNumerals[$compareItem];
            
            } 
            else {
    
                $isgreaterthan = false;
            }
        } 
    }

    //sorting array bacwards 
    krsort($romanNumerals);

    while($num = current($romanNumerals)) {

        if($number >= key($romanNumerals)) {

            Changer(key($romanNumerals));
        }

        next($romanNumerals);
    }

    echo $romanNumber;    